# ImageExtras

Utilities and compiled algorithms to make Image Processing easier

## Getting started

To install it, you can either clone the repository and install it locally:
```bash
cd imageextras
python setup.py sdist bdist_wheel
pip install .
```

Or you can install it through the official PyPI repository:
```bash
pip install ImageExtras
```
